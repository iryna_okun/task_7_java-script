const title = document.createElement ('h1');
title.innerHTML = "Asynchronus JavaScript";
document.body.appendChild(title);

//TASK 2.1
test ('quantity of burst bubbles', () => {
    expect (score ()).toBe (20);
});



// TASK 2.2

var string = “mic09ha1el 4b5en6 michelle be4atr3ice” 
var strArray = “mic09ha1el 4b5en6 michelle be4atr3ice”.split(“ “);
strArray.superSort((a-b) => a-b);
console.log (strArray)




//   TASK 2.3. Variant 1

/* 
var date1 = '01/12/2020'
var date2 = '2020/10/17'

date 1 = new Date (date 1);
date 2 = new Date (date 2);

date1 > date2;
date1 < date2;
date1 >= date2;
date1 <= date2;
console.log(date1 > date2);

Variant 2
*/ 

function compare(dateTimeA, dateTimeB) {
    var momentA = moment(dateTimeA,"DD/MM/YYYY");
    var momentB = moment(dateTimeB,"YYYY/MM/DD");
    if (momentA > momentB) return 1;
    else if (momentA >= momentB) return -1;
    else if (momentA < momentB) return -1;
    else if (momentA <= momentB) return -1;
    else return 0;
}

alert(compare("2014/10/17", "11/04/2018"));

    console.log(compare);


// TASK 2.4

var a =  [ 2, 2, 9, 2, 2].reduce(function(map,el){
    map[el] = (map[el]||0)+1;
    return map;
},{});

var filtered = Object.keys(a).filter(function(el){
  return a[el] == 1;
})

console.log(filtered);

// TASK 2.5

var getDiscount = 0;
var price = 23;
var quantity = 7;
var total = quantity*price;

if (quantity < 5) {
    getDiscount = 0;
    total = total - (total*getDiscount);
    return (total);
}
else if (quantity > 5 || quantity < 10) {
    getDiscount = 0.05;
    total = total - (total*getDiscount);
    return (total);
}
else if (quantity >= 10) {
    getDiscount = 0.1;
    total = total - (total*getDiscount);
    return (total);
}


// TASK 2.6

//  без учета високосного года
var year = 2020;
var month = 7;
var getDaysForMonth;
switch (month) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
        getDaysForMonth = 31;
        break;
    case 2:
    case 4:
    case 6:
    case 9:
    case 11:
        getDaysForMonth = 30;
}

console.log(getDaysForMonth);

// с учетом високосного года

var year = 2020;
var month = 2;
var getDaysForMonth;
switch (month) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
        getDaysForMonth = 31;
        break;
    case 4:
    case 6:
    case 9:
    case 11:
        getDaysForMonth = 30;
        break;
    case 2:
        if (((year % 4 == 0) && !(year % 100 == 0))
            || (year % 400 == 0))
            getDaysForMonth = 29;
        else
            getDaysForMonth = 28;
        break;
    default:
        getDaysForMonth = -1; // invalid month
}

console.log(getDaysForMonth); // 29







