const API_URL = 'https://api.github.com/';
const rootElement = document.getElementById('root');
const loadingElement = document.getElementById('loading-overlay');
const fightersDetailsMap = new Map();


  function callApi(endpoind, method) {
    const url = API_URL + endpoind
    const options = {
      method
    };
  
    return fetch(url, options)
      .then(response => 
        response.ok 
          ? response.json() 
          : Promise.reject(Error('Failed to load'))
      )
      .catch(error => { throw error });
  }

  class FighterService {
    async getFighters() {
      try {
        const endpoint = 'repos/sahanr/street-fighter/contents/fighters.json';
        const apiResult = await callApi(endpoint, 'GET');
  
        return JSON.parse(atob(apiResult.content));
      } catch (error) {
        throw error;
      }
    }
  }
  
  const fighterService = new FighterService();

  class View {
    element;
  
    createElement({ tagName, className = '', attributes = {} }) {
      const element = document.createElement(tagName);
      element.classList.add(className);
      
      Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));
  
      return element;
    }
  }

  class FighterView extends View {
    constructor(fighter, handleClick) {
      super();
  
      this.createFighter(fighter, handleClick);
    }
  
    createFighter(fighter, handleClick) {
      const { name, source } = fighter;
      const nameElement = this.createName(name);
      const imageElement = this.createImage(source);
  
      this.element = this.createElement({ tagName: 'div', className: 'fighter' });
      this.element.append(imageElement, nameElement);
      this.element.addEventListener('click', event => handleClick(event, fighter), false);
    }
  
    createName(name) {
      const nameElement = this.createElement({ tagName: 'span', className: 'name' });
      nameElement.innerText = name;
  
      return nameElement;
    }
  
    createImage(source) {
      const attributes = { src: source };
      const imgElement = this.createElement({
        tagName: 'img',
        className: 'fighter-image',
        attributes
      });
  
      return imgElement;
    }
  }
  
  class FightersView extends View {
    constructor(fighters) {
      super();
      
      this.handleClick = this.handleFighterClick.bind(this);
      this.createFighters(fighters);
    }
  
    fightersDetailsMap = new Map();
  
    createFighters(fighters) {
      const fighterElements = fighters.map(fighter => {
        const fighterView = new FighterView(fighter, this.handleClick);
        return fighterView.element;
      });
  
      this.element = this.createElement({ tagName: 'div', className: 'fighters' });
      this.element.append(...fighterElements);
    }
  
    handleFighterClick(event, fighter) {
      this.fightersDetailsMap.set(fighter._id, fighter);
      console.log('clicked')
      // get from map or load info and add to fightersMap
      // show modal with fighter info
      // allow to edit health and power in this modal
    }
  }

  class App {
    constructor() {
      this.startApp();
    }
  
    static rootElement = document.getElementById('root');
    static loadingElement = document.getElementById('loading-overlay');
  
    async startApp() {
      try {
        App.loadingElement.style.visibility = 'visible';
        
        const fighters = await fighterService.getFighters();
        const fightersView = new FightersView(fighters);
        const fightersElement = fightersView.element;
  
        App.rootElement.appendChild(fightersElement);
      } catch (error) {
        console.warn(error);
        App.rootElement.innerText = 'Failed to load data';
      } finally {
        App.loadingElement.style.visibility = 'hidden';
      }
    }
  }
  
  new App();



// Hometask 1.2

// variant 1 

  function MyFighter(name, age, height, weight) {
    this.name = name;
    this.age = age;
    this.height = height;
    this.weight = weight;
    this.getName = function() {
      // Используем ключевое слово this для ссылки на свойство name объекта Fighter
      return this.name;
     }
    }

    let Ryu = new MyFighter('Ryu', 20, 185, 85);
    let Dhalsim = new MyFighter('Dhalsim', 40, 170, 78);
    let Guile = new MyFighter('Guile', 35, 190, 90);
    let Zangief = new MyFighter('Zangief', 50, 210, 140);
    let Ken = new MyFighter('Ken', 20, 180, 80);
    let Bison = new MyFighter('Bison', 45, 188, 100);
    
    console.log(Ryu.getName()); 
    console.log(Dhalsim.getName()); 
    console.log(Guile.getName()); 
    console.log(Zangief.getName()); 
    console.log(Ken.getName()); 
    console.log(Bison.getName());

    //....

    /* variant 2 (ostavila variant 1, etot dlya sebja zakommentirovala)

    function MyFighterB (name, age, weight, height) {
      this.name = name;
      this.age = age;
      this.weight = weight;
      this.height = height;
      
      const Ryu = new MyFighterB ('Ryu', 20, 185, 85);
      const Dhalsim = new MyFighterB ('Dhalsim', 40, 170, 78);
      const Guile = new MyFighterB ('Guile', 35, 190, 90);
      const Zangief = new MyFighterB ('Zangief', 50, 210, 140);
      const Ken = new MyFighterB ('Ken', 20, 180, 80);
      const Bison = new MyFighterB ('Bison', 45, 188, 100);
          
          MyFighterB.prototype.info = function () {
          console.log '${this.name} ${this.age} ${this.weight} ${this.height}';   
        }
    };

    Ryu.info();
    Dhalsim.info();
    Guile.info();
    Zangief.info();
    Ken.info();
    Bison.info();

*/
    // * Hometask 1.3

    class MyFighterC {
      constructor (name, health, attack, defense) {
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.defense = defense;
      }
    }

    const myfighter1 = new MyFighterC (Ryu, 100, 100, 99);
    const myfighter2 = new MyFighterC (Dhalsim, 80, 56, 59);
    const myfighter3 = new MyFighterC (Guile, 85, 98, 68);
    const myfighter4 = new MyFighterC (Zangief, 95, 72, 92);
    const myfighter5 = new MyFighterC (Ken, 70, 82, 95);
    const myfighter6 = new MyFighterC (Bison, 100, 100, 90);
    console.log (myfighter1);
    console.log (myfighter2);
    console.log (myfighter3);
    console.log (myfighter5);
    console.log (myfighter6);



    class MyFighterD {
      constructor (CriticalHitChance, attack) {
        this.CriticalHitChance = 1||2;
      }

      get attack () {
        return this.attack ();
      }

      getHitPower (){
        return this.calcgetHitPower;
      }

      calcgetHitPower (){
        return this.CriticalHitChance*this.attack;
      }
    } 
    
    const myfighter11 = new MyFighterD (5, 100);
    console.log(myfighter11, getHitPower);



    class MyFighterE {
      constructor (dodgeChance, defense) {
        this.dodgeChance = 1||2;
      }

      get defense () {
        return this.defense ();
      }

      getBlockPower (){
        return this.calcgetBlockPower;
      }

      calcgetBlockPower (){
        return this.dodgeChance*this.defense;
      }
    } 

    const myfighter111 = new MyFighterE (2, 99);
    console.log(myfighter111, getBlockPower);



    




